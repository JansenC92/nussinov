#!/usr/bin/python3

import sys
import re
from argparse import ArgumentParser

from nussinov_functions import *


parser = ArgumentParser()

parser.add_argument("-i", "--input_file",
                    default="STDIN",
                    help="Fasta input file with RNA-sequence. Default reads from STDIN.")  # todo input DONE
parser.add_argument("-o", "--output_file",
                    default="STDOUT",
                    help="Outputfile to save to. Default: STDOUT")  # todo output DONE
parser.add_argument("-l", "--min_loop",
                    default=4,
                    help="Minimum loop size between matching bases. Default: 4")  # todo loop length DONE
parser.add_argument("-s", "--show_seq",
                    action="store_true",
                    default=False,
                    help="Flag to show entered sequence.")  # todo show sequence
args = parser.parse_args()


### manage input ###
is_header = re.compile('^>')
sequence_dict = {}

if args.input_file == "STDIN":
    # if not sys.stdin.isatty():
    #     for line in sys.stdin:
    #         if is_header.match(line):
    #             tmp = line.rstrip("\n")
    #             header_list.append(tmp)
    #         else:
    #             tmp = line.upper().rstrip("\n")
    #             sequence_list.append(tmp)

    if not sys.stdin.isatty():
        sequence = ""
        header = ""
        for line in sys.stdin:
            if is_header.match(line):
                header = line.rstrip("\n")
            else:
                sequence = line.upper().rstrip("\n")
            if is_RNA(sequence):
                sequence_dict.update({header: sequence})
            else:
                next(sys.stdin, None)
                continue
    else:
        print("ERROR! No input file given! \n")
        parser.print_help()
else:
    with open(args.input_file) as fp:
        sequence = ""
        header = ""
        for line in fp:
            if is_header.match(line):
                header = line.rstrip("\n")
            else:
                sequence = line.upper().rstrip("\n")
                if is_RNA(sequence):
                    sequence_dict.update({header: sequence})
                else:
                    next(fp, None)
                    continue


### manage output ###
def print_to_file(out_string):
    """
    Handles the output of nussinov_main.py and prints either to a given file or to stdout
    :param out_string: str
    :return: Void
    """
    if args.output_file == "STDOUT":
        print(out_string)
    else:
        with open(args.output_file, "a") as out_file:
            print(out_string, file=out_file)


### manage minimal loop size ###
min_loop = int(args.min_loop)


def main():
    bracket_list = []
    for header, sequence in sequence_dict.items():
        bracket_list.append(run_nussinov(sequence, min_loop))
    header_list = list(sequence_dict.keys())
    sequence_list = list(sequence_dict.values())
    result = return_fasta_style(header_list, sequence_list, bracket_list)
    print_to_file(result)


if __name__ == '__main__':
    main()
