#!/usr/bin/python3
import numpy as np
import sys
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument("-i","--input_file", default="STDIN", help="Fasta input file with RNA-sequence. Default reads from STDIN.")
parser.add_argument("-p","--output_file", default="STDOUT", help="Outputfile to save to. Default: STDOUT")
parser.add_argument("-l","--min_loop", default=4, help="Minimum loop size inbetween matching bases. Default: 4")
parser.add_argument("-s","--show_seq", action="store_true", default=False, help="Flag to show entered sequence.")
args = parser.parse_args()

if args.input_file == "STDIN":
    seq = sys.stdin.read()
    seq = seq.upper()
else:
    print("NOT YET IMPLEMENTED")
    exit(1)

min_loop = int(args.min_loop)

#seq = "UAGCUCAGUUGGGAGAGCG"
#seq = "GCGGAUUUAGCUCAGUUGGGAGAGCGCCAGACUGAAGAUUUGGAGGUCCUGUGUUCGAUCCACAGAAUUCGCA"
if args.show_seq:
    print(seq)
row = col = len(seq)

nus_mat = np.zeros(shape=(row, col), dtype=np.int)

for i in range(row):
    for j in range(col):
        if i < j:
            nus_mat[i][j] = -1

def match(X,Y):
    matches = ["AU","UA","GC","CG"]
    if X + Y in matches:
        return True


def nussinov(i, j, seq=seq, matrix=nus_mat):

    if matrix[i][j] != -1:
        return matrix[i][j]

    if i >= j - min_loop:
        return 0

    max_value = matrix[i][j-1]

    for k in np.arange(start=i, stop=j, step=1):
        if match(seq[k], seq[j]):
            x = nussinov(matrix=matrix, i=i, j=k - 1) + nussinov(matrix=matrix, i=k + 1, j=j - 1) + 1
            if x > max_value:
                max_value = x

    return max_value



def trace_back(i,j, matrix, result):
    if j <= i:
        return
    elif matrix[i][j] == matrix[i][j-1]:
        trace_back(i=i, j=(j-1), matrix=matrix, result=result)

    else:
        for k in np.arange(start=i, stop=j-min_loop):
            if match(seq[k], seq[j]):
                if matrix[i][j] == matrix[i][k-1] + matrix[k+1][j-1] + 1:
                    result.append((k,j))
                    trace_back(i,k-1, matrix=matrix, result=result)
                    trace_back(k+1, j-1, matrix=matrix, result=result)
                    break

def write_result(sequence, result):
    bracket_notation = ["."] * len(sequence)
    for pair in result:
        bracket_notation[min(pair)] = "("
        bracket_notation[max(pair)] = ")"
    return "".join(bracket_notation)


for ri in range(row):
    for ci in range(col):
        nus_mat[ri][ci] = nussinov(i=ri, j=ci, matrix=nus_mat, seq=seq)


structure = []
trace_back(0, col-1, matrix=nus_mat, result=structure)

if args.output_file == "STDOUT":
    print(write_result(seq, structure))
else:
    print("NOT YET IMPLEMENTED")
    exit(1)
