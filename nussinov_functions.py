import numpy as np


def is_RNA(sequence):
    """
    Check if string is RNA sequence
    :param sequence: str
    :return: bool
    """
    alphabet = "ACGU"
    sequence = sequence.upper()
    is_rna = all(char in alphabet for char in sequence)
    return is_rna



def initialize_matrix(sequence):
    """
    Create and initialize a matrix with dimensions sequence-length x sequence-length
    :param sequence: str
    :return: numpy matrix
    """
    row = col = len(sequence)
    nus_mat = np.zeros(shape=(row, col), dtype=np.int)
    for i in range(row):
        for j in range(col):
            if i < j:
                nus_mat[i][j] = -1
    return nus_mat


def match(x, y):
    """
    Check if a string of 2 characters x+y matches an alphabet
    :param x: char
    :param y: char
    :return: bool
    """
    matches = ["AU", "UA", "GC", "CG"]
    if x + y in matches:
        return True


def nussinov(i, j, seq, matrix, min_loop):
    """
    Calculate one value of the nussinov scoring matrix
    :param i: int; vertical position
    :param j: int; horizontal position
    :param seq: string
    :param matrix: numpy matrix
    :param min_loop: int
    :return: int | float
    """
    if matrix[i][j] != -1:
        return matrix[i][j]

    if i >= j - min_loop:
        return 0

    max_value = matrix[i][j-1]

    for k in np.arange(start=i, stop=j, step=1):
        if match(seq[k], seq[j]):
            tmp = nussinov(seq=seq, matrix=matrix, i=i, j=k - 1, min_loop=min_loop) + \
                  nussinov(seq=seq, matrix=matrix, i=k + 1, j=j - 1, min_loop=min_loop) + 1
            if tmp > max_value:
                max_value = tmp
    return max_value


def fill_scoring_matrix(seq, matrix, min_loop):
    """
    Loops over a sequence, calculates the nussinov scores and writes them into the matrix
    :param seq: str
    :param matrix: numpy matrix
    :param min_loop: int
    :return: numpy matrix
    """
    row = col = len(seq)
    for ri in range(row):
        for ci in range(col):
            matrix[ri][ci] = nussinov(i=ri, j=ci, matrix=matrix, seq=seq, min_loop=min_loop)
    return matrix


def trace_back(i, j, matrix, result_list, seq, min_loop):
    """
    Traces back the optimal alignment and saves the base pair positions in tuples
    :param i: int; vertical position
    :param j: int; horizontal position
    :param matrix: numpy matrix
    :param result_list: empty list (added from outer namespace because of the recursion)
    :param seq: str
    :param min_loop: int
    :return: None
    """
    if j <= i:
        return
    elif matrix[i][j] == matrix[i][j-1]:
        trace_back(i=i, j=(j-1), matrix=matrix, result_list=result_list, seq=seq, min_loop=min_loop)

    else:
        for k in np.arange(start=i, stop=j-min_loop):
            if match(seq[k], seq[j]):
                if matrix[i][j] == matrix[i][k-1] + matrix[k+1][j-1] + 1:
                    result_list.append((k, j))
                    trace_back(i, k - 1, matrix=matrix, result_list=result_list, seq=seq, min_loop=min_loop)
                    trace_back(k + 1, j - 1, matrix=matrix, result_list=result_list, seq=seq, min_loop=min_loop)
                    break


def write_traceback(seq, matrix, min_loop):
    """
    Run traceback and return result
    :param seq: str
    :param matrix: numpy matrix
    :param min_loop: int
    :return: list of tuples
    """
    col = len(seq)
    structure = []
    trace_back(0, col - 1, matrix=matrix, result_list=structure, seq=seq, min_loop=min_loop)
    return structure


def write_bracket_notation(seq, result_list):
    """
    Reads sequence and writes "." if base is not paired and "(" or ")" if base is paired
    :param seq: str
    :param result_list: list of tuples
    :return: str
    """
    bracket_notation = ["."] * len(seq)
    for pair in result_list:
        bracket_notation[min(pair)] = "("
        bracket_notation[max(pair)] = ")"
    return "".join(bracket_notation)


def run_nussinov(seq, min_loop):
    """
    Run all functions of nussinov algorithm and return result in bracket notation
    :param seq: str
    :param min_loop: int
    :return: str
    """
    scoring_matrix = initialize_matrix(seq)
    scoring_matrix = fill_scoring_matrix(seq, scoring_matrix, min_loop)
    base_pair_positions = write_traceback(seq, scoring_matrix, min_loop)
    bracket_string = write_bracket_notation(seq, base_pair_positions)
    return bracket_string


def return_fasta_style(header_list, sequence_list, bracket_list):
    """
    Create a string with header, sequence and bracket notation in fasta style
    :param header_list: list of strings
    :param sequence_list: list of strings
    :param bracket_list: list of strings
    :return: str
    """
    result = ""
    if len(header_list) == len(sequence_list) == len(bracket_list):
        length = len(header_list)
        for i in range(0, length):
            result += f"{header_list[i]}\n" \
                f"{sequence_list[i]}\n" \
                f"{bracket_list[i]}\n"
    return result


def main():
    sequence = "ACUCGAUUCCGAG"
    min_loop = 4
    scoring_matrix = initialize_matrix(sequence)
    scoring_matrix = fill_scoring_matrix(sequence, scoring_matrix, min_loop)
    base_pair_positions = write_traceback(sequence, scoring_matrix, min_loop)
    bracket_string = write_bracket_notation(sequence, base_pair_positions)
    print(sequence)
    print(bracket_string)


if __name__ == '__main__':
    main()
